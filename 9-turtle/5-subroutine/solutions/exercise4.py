from turtle import *

speed("fastest")

def bogen():
    for i in range(90):
        right(1)
        forward(2)

def blumenblatt():
    pencolor("red")
    fillcolor("red")
    begin_fill()
    bogen()
    right(90)
    bogen()
    end_fill()
    right(90)

def stiel():
    tracer(1)
    right(45)
    pencolor("black")
    pensize(3)
    pendown()
    for i in range(20):
        right(2)
        forward(15)

def blume():
    for i in range(5):
        blumenblatt()
        left(72)
    stiel()

blume()

