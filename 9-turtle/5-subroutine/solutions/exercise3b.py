from turtle import *

speed("fastest")

def bogen():
    for i in range(90):
        right(1)
        forward(2)

def blumenblatt():
    bogen()
    right(90)
    bogen()
    right(90)

blumenblatt()
