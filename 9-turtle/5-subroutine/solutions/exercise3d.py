from turtle import *

speed("fastest")

def bogen():
    for i in range(90):
        right(1)
        forward(2)

def blumenblatt():
    pencolor("red")
    fillcolor("red")
    begin_fill()
    bogen()
    right(90)
    bogen()
    end_fill()
    right(90)

def blume():
    for i in range(5):
        blumenblatt()
        left(72)

blume()

