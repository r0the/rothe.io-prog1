from turtle import *

def quadrat():
    right(45)
    for i in range(4):
        forward(71)
        left(90)
    left(45)
    penup()
    forward(100)
    pendown()

pencolor("blue")
quadrat()
pencolor("red")
quadrat()
pencolor("green")
quadrat()
pencolor("black")
quadrat()

hideturtle()
