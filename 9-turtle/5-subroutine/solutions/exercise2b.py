from turtle import *

def quadrat():
    right(45)
    begin_fill()
    for i in range(4):
        forward(71)
        left(90)
    end_fill()
    left(45)
    penup()
    forward(100)
    pendown()

pencolor("blue")
fillcolor("blue")
quadrat()
pencolor("red")
fillcolor("red")
quadrat()
pencolor("green")
fillcolor("green")
quadrat()
pencolor("black")
fillcolor("black")
quadrat()

hideturtle()
