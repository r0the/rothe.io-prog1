from turtle import *

def quadrat():
    for i in range(4):
        forward(100) 
        left(90) 

pencolor("red")
quadrat()

right(120)
pencolor("blue")
quadrat()

right(120)
pencolor("green")
quadrat()
