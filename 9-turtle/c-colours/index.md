# :mdi-bookshelf: Farben

## Farben festlegen

Es gibt zwei Möglichkeiten, eine Farbe für die Python-Turtle festzulegen. Entweder wird ein vordefinierter Name für eine Farbe verwendet oder es wird ein RGB-Wert angegeben.

### RGB-Werte

Wir wissen, das im Computer Farben im RGB-Farbmodell dargestellt werden. Dabei wird für die Grundfarben Rot, Grün und Blau jeweils eine Helligkeit festgelegt.

Die Turtle akzeptiert ebenfalls RGB-Werte als Farbe. Der RGB-Wert kann z.B. mit dem folgenden Tool bestimmt werden:

<VueColourPicker percent output="tuple"/>

Das Farbtupel kann nun in den Turtle-Befehlen `pencolor` oder `fillcolor` verwendet werden:

``` python
fillcolor(1.0, 0.8, 0.5)
```


### Farbnamen

Um eine Farbe über einen Namen festzulegen, muss der Name der Farbe in doppelten Anführungszeichen angegeben werden:

``` python
fillcolor("light sky blue")
```

Die folgende Tabelle zeigt eine Auswahl von möglichen Namen für Farben:

<VueColourTable/>
