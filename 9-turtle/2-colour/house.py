# Zeichnet ein Haus

from turtle import *

pencolor("sandy brown") # Linienfarbe
fillcolor("sandy brown") # Flächenfarbe
begin_fill()
forward(100)
left(90)
forward(100)
left(45)
forward(72)
left(90)
forward(72)
left(45)
end_fill() # Hier wird ausgefüllt
hideturtle()
