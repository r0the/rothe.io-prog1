from turtle import *

fillcolor("black")
begin_fill()
forward(80)
left(90)
forward(220)
left(90)
forward(80)
end_fill()

penup()

back(40)
left(90)
forward(50)

pendown()
pencolor("red")
dot(40)

penup()
forward(60)

pendown()
pencolor("yellow")
dot(40)

penup()
forward(60)

pendown()
pencolor("green1")
dot(40)

hideturtle()

