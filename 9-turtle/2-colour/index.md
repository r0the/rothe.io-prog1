# 9.2 Farben verwenden
---

## Einführung

Um ihre Spur zu zeichnen, hat die Turtle einen Farbstift (engl. pen). Für diesen Farbstift kennt die Turtle weitere Anweisungen. Solange der Farbstift «unten» ist, zeichnet die Turtle eine Spur. Mit `penup()` nimmt sie den Farbstift nach oben und bewegt sich nun, ohne eine Spur zu zeichnen. Mit `pendown()` wird der Farbstift wieder nach unten auf die Zeichenfläche gebracht, so dass eine Spur gezeichnet wird.

Über die Anweisung `pencolor(farbe)` kannst du die Farbe des Stifts auswählen. Wichtig ist, dass du den Farbnamen in Anführungszeichen setzt. Wie immer beim Programmieren kennt die Turtle nur englische Farbnamen. Die folgende Liste ist zwar nicht vollständig, aber doch ein erster Anhaltspunkt:

<VueColourTable name="simple-colours"/>

Programmierkonzepte: Programmgesteuertes Zeichnen

## Farbe und Breite des Stifts

Mit einer breiten roten Linie zeichnet die Turtle eine Kerze. Die Linienbreite in Pixel kannst du mit dem Befehl `pensize()` einstellen.

Die gelbe Flamme kannst du mit dem Befehle `dot()` zeichnen. Dazwischen ist ein Programmteil, in dem sich die Turtle zwar bewegt, aber keine Linie zeichnet, weil der Stift mit `penup()` gehoben wurde. Nach `pendown()` zeichnet die Turtle wieder.

`hideturtle()` macht die Turtle unsichtbar.

::: columns 2
``` python candle.py
```
***
![](./candle.png)
:::

::: info

~~~ python
penup()
~~~
hebt den Stift an. Wenn sich die Turtle nun bewegt, zeichnet sie nicht mehr.

~~~ python
pendown()
~~~
senkt den Stift. Wenn sich die Turtle nun bewegt, zeichnet sie wieder.

~~~ python
pencolor(<mark>farbe</mark>)
~~~
legt die Zeichenfarbe für den Stift fest. Die Farbe muss in Anführungszeichen geschrieben werden. Auf der Seite [Farben](?page=../colours/) findest du eine Erklärung, wie Farben angegeben werden können.

~~~ python
pensize(<mark>n</mark>)
~~~
legt die Dicke des Zeichenstifts auf `n` Pixel fest.
:::

## Gefüllte Flächen

Du kannst mit der Turtle fast beliebige Flächen mit Farben füllen. Mit dem Befehl `begin_fill()` sagst du der Turtle, dass du die Absicht hast, eine Fläche zu füllen. Die Turtle merkt sich dabei die momentane Lage als Startpunkt eines Linienzugs. Du umfährst dann mit der Turtle die Fläche und befiehlst ihr zuletzt mit `end_fill()`, denn erreichten Endpunkt des Linienzugs mit dem Startpunkt zu verbinden und die so entstandene Fläche zu füllen. Die Füllfarbe kannst du mit `fillcolor(farbe)` einstellen.

Texte einer Zeile nach einer einleitenden Raute `#` sind **Kommentare**. Sie werden bei der Programmausführung nicht beachtet. Du kannst dir zum Beispiel notieren, unter welchen Programmnamen die Datei gespeichert ist oder erklärenden Text zum Programmcode schreiben.

::: columns 2
``` python house.py
```
***
![](./house.png)
:::

::: info
~~~ python
fillcolor(<mark>farbe</mark>)
~~~
legt die Farbe für das Füllen von Figuren fest. Die Farbe muss in Anführungszeichen geschrieben werden. Auf der Seite [Farben](?page=../colours/) findest du eine Erklärung, wie Farben angegeben werden können.

~~~ python
begin_fill()
~~~
beginnt damit, eine gefüllte Figur zu zeichnen.

~~~ python
end_fill()
~~~
beendet die Figur und füllt sie aus.

**Kommentare** werden mit dem Zeichnen `#` eingeleitet.
:::


::: exercise
#### :exercise: Aufgabe 1 – Sechseck
Zeichne mit der Turtle ein regelmässiges Sechseck und wähle für jede Seite eine andere Farbe.

![](./exercise1.png)
***
``` python solutions/exercise1.py
```
:::

::: exercise
#### :exercise: Aufgabe 2 – Ampel
Zeichne eine Ampel. Das schwarze Rechteck kannst du mit der Breite 80 zeichnen, die Kreisflächen mit `dot(40)`.

![](./exercise2.png)
***
``` python solutions/exercise2.py
```
:::

::: exercise
#### :exercise: Aufgabe 3 – Dreiecke
Die Turtle soll das untenstehende Bild zeichnen.

![](./exercise3.png)
***
``` python solutions/exercise3.py
```
:::
