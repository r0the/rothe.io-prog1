# 9.4 Umgang mit Fehlern
---

## Fehler erkennen

Da Python-Programme schrittweise ausgeführt werden, kann es sein, dass das Programm plötzlich abbricht, weil Python auf einer Zeile ein Fehler entdeckt hat. In diesem Fall wird in der __Shell__ von Thonny eine Fehlermeldung ausgegeben:

![Python zeigt in der Shell von Thonny einen Fehler an](./thonny-error.png)

In der Fehlermeldung lassen sich wichtige Informationen erkennen:

- Auf der letzten Zeile steht die eigentliche Fehlermeldung, im Beispiel oben _NameError: name 'Forward' is not defined_.
- Auf der vorletzten Zeile steht, in welchem Python-Programm und auf welcher Zeile der Fehler aufgetreten ist, im Beispiel oben in _turtle_fehler.py_ auf der Zeile _3_.

Mit einem Klick auf den Link kann man direkt zur Zeile springen, welche den Fehler verursacht hat.

Thonny zeigt zusätzlich im Bereich __Assistant__ eine Erklärung zum Fehler und mögliche Fehlerursachen an. Der Bereich kann über das Menü _View ‣ Assistant_ eingeblendet werden.

## Häufige Fehler

### NameError

_NameError: name '…' is not defined_

Der angegebene Name ist Python nicht bekannt. Typische Szenarien sind:

- Turtle-Befehle werden verwendet, aber das Turtle-Modul wird nicht importiert:

    ``` python
    forward(100)
    ```

    Korrektur:

    ``` python
    from turtle import *
    forward(100)
    ```

- Der Grossschreibung eines Befehls ist falsch (`Forward` statt `forward`):

    ``` python
    from turtle import *
    Forward(100)
    ```

    Korrektur:

    ``` python
    from turtle import *
    forward(100)
    ```

- Der Rechtschreibung eines Befehls ist falsch (`pencolour` statt `pencolor`). In Programmiersprachen wird üblicherweise die amerikanische Rechtschreibung verwendet:

    ``` python
    from turtle import *
    pencolour(100)
    ```

    Korrektur:

    ``` python
    from turtle import *
    pencolor(100)
    ```

### IndentationError

_IndentationError: unexpected indent_

Vor einem Befehl steht ein Leerzeichen. Das ist nicht erlaubt, das Einrücken von Befehlen hat eine spezielle Bedeutung in Python. Typische Szenarien sind:

- Eine Zeile beginnt mit einem Leerzeichen:

    ``` python
    from turtle import *
     forward(100)
    ```

    Korrektur:

    ``` python
    from turtle import *
    forward(100)
    ```

_IndentationError: expected an indented block_

- Nach einer `for`- oder `def`-Anweisung fehlt ein eingerückter Befehl:

    ``` python
    from turtle import *
    for i in range(4):
    forward(100)
    ```

    Korrektur:

    ``` python
    from turtle import *
    for i in range(4):
        forward(100)
    ```

### SyntaxError

_SyntaxError: invalid syntax_

Normalerweise ein Fehler mit der Interpunktion. Es fehlt ein Zeichen, z.B. ein Gleichheitszeichen `=` oder ein Punkt `.`

- Am Ende eine `for`- oder `def`-Anweisung fehlt ein Doppelpunkt:

    ``` python
    from turtle import *
    for i in range(4)
        forward(100)
    ```

    Korrektur:

    ``` python
    from turtle import *
    for i in range(4):
        forward(100)
    ```

### TurtleGraphicsError


_turtle.TurtleGraphicsError: bad color string: …_

Im Turtle-Befehl `fillcolor` oder `pencolor` wird ein Namen für eine Farbe angegeben, welche von Python nicht verstanden wird. Beispiel:

- Ein Namen einer Farbe wird falsch geschrieben:

    ``` python
    from turtle import *
    pencolor("dark tortoise")
    ```

    Korrektur:

    ``` python
    from turtle import *
    pencolor("dark turquoise")
    ```

_turtle.TurtleGraphicsError: bad color sequence: (…, …, …)_

Im Turtle-Befehl `fillcolor` oder `pencolor` wird eine RGB-Farbe angegeben, welches von Python nicht verstanden wird. Beispiel:

- Es wird ein Rot-, Grün- oder Blauwert angegeben, der nicht zwischen 0 und 1 liegt.

    ``` python
    from turtle import *
    pencolor(2, 1, 0)
    ```

    Korrektur:

    ``` python
    from turtle import *
    pencolor(1, 0.5, 0)
    ```

::: exercise
#### :exercise: Aufgabe – Flagge der Schweiz

![](./schweiz.png)

1. Kopieren Sie das folgende Python-Programm nach Thonny und speichern Sie es als _schweiz.py_.
2. Das Programm sollte die Flagge der Schweiz zeichnen, aber es enthält Fehler. Korrigieren Sie alle Fehler.
3. Obschon das Programm fehlerfrei läuft, wird das weisse Kreuz nicht ausgefüllt. Wieso? Können Sie den Fehler finden?

``` python ./schweiz.py
```

:::
