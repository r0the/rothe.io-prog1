from turtel import *

def rechteck(x, y, farbe)
    goto(x, y)
    fillcolour(farbe)
    pendown()
    begin_fill()
    for i inrange(2):
        forward(300)
        left(90)
        forward(50)
        left(90)
    end_fill()
    penup()

penup()
 rechteck(-150, -75, "rot")
rechteck(-150, -25, "blau")
rechteck(-150, 25, "rot")

fillcolor("weiss")
penup()
goto(0, -40)
pendown()
begin_fill()
circle(40, 360)
end_fill
