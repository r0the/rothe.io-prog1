from turtel import *

fillcolor("rot")
 penup()
begin_fill()
for i in range(4)
    forward(250)
    left(90)
end_fill()
forward(150)
links(90)
forward(50)
fillcolor("white")
begin_fill()
for i in range(4):
    forward(50)
    right(90)
    forward(50)
    left(90)
    forward(50)
    left(90)
end_fill

    