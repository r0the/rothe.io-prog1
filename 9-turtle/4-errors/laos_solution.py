from turtle import *

def rechteck(x, y, farbe):
    goto(x, y)
    fillcolor(farbe)
    pendown()
    begin_fill()
    for i in range(2):
        forward(300)
        left(90)
        forward(50)
        left(90)
    end_fill()
    penup()

penup()
rechteck(-150, -75, "red")
rechteck(-150, -25, "blue")
rechteck(-150, 25, "red")

fillcolor("white")
penup()
goto(0, -40)
pendown()
begin_fill()
circle(40, 360)
end_fill()
