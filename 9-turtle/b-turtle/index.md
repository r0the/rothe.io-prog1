# :mdi-bookshelf: Turtle-Befehle
---

## Bewegung

~~~ python
forward(<mark>n</mark>)
~~~
bewegt die Turtle um `n` Pixel nach vorne.

~~~ python
back(<mark>n</mark>)
~~~
bewegt die Turtle um `n` Pixel nach hinten.

~~~ python
left(<mark>a</mark>)
~~~
dreht die Turtle um `a` Grad nach links.

~~~ python
right(<mark>a</mark>)
~~~
dreht die Turtle um `a` Grad nach rechts.

~~~ python
goto(<mark>x</mark>, <mark>y</mark>)
~~~
bewegt die Turtle zu den Koordinaten (`x`, `y`).

~~~ python
distance(<mark>x</mark>, <mark>y</mark>)
~~~
berechnet die Distanz von der Turtle zu den Koordinaten (`x`, `y`).

## Stift

~~~ python
penup()
~~~
hebt den Stift an. Wenn sich die Turtle nun bewegt, zeichnet sie nicht mehr.

~~~ python
pendown()
~~~
senkt den Stift. Wenn sich die Turtle nun bewegt, zeichnet sie wieder.

~~~ python
isdown()
~~~
überprüft, ob der Stift gesenkt ist.

~~~ python
pencolor(<mark>farbe</mark>)
~~~
legt die Zeichenfarbe für den Stift fest. Die Farbe muss in Anführungszeichen geschrieben werden. [Hier][4] ist eine Liste von Farben.

~~~ python
pensize(<mark>dicke</mark>)
~~~
legt die Dicke des Zeichenstifts in Pixel fest.

~~~ python
dot(<mark>radius</mark>)
~~~
zeichnet an der aktuellen Position einen Kreis mit dem angegebenen Radius.

~~~ python
circle(<mark>radius</mark>)
~~~
zeichnet mit der Turtle einen Kreis mit dem angegebenen Radius.

~~~ python
circle(<mark>radius</mark>, <mark>winkel</mark>)
~~~
zeichnet mit der Turtle einen Kreisbogen mit dem angegebenen Radius und Winkel. Mit einem Winkel von 180 wird also ein Halbkreis gezeichnet.

## Füllen

~~~ python
fillcolor(<mark>farbe</mark>)
~~~
legt die Farbe für das Füllen von Figuren fest. Die Farbe muss in Anführungszeichen geschrieben werden. [Hier][4] ist eine Liste von Farben.

~~~ python
begin_fill()
~~~
beginnt damit, eine gefüllte Figur zu zeichnen.

~~~ python
end_fill()
~~~
beendet die Figur und füllt sie aus.

## Aussehen und Geschwindigkeit

~~~ python
shape(<mark>aussehen</mark>)
~~~
ändert das Aussehen der Turtle. Für `aussehen` können folgende Werte eingesetzt werden: `"arrow"`, `"turtle"`, `"circle"`, `"square"`, `"triangle"`, `"classic"`.

~~~ python
hideturtle()
~~~
versteckt die Turtle. Kann verwendet werden, um die Turtle nach dem Zeichnen aus ihrem Kunstwerk zu verbannen.

~~~ python
showturtle()
~~~
zeigt die Turtle wieder an.

~~~ python
speed(<mark>stufe</mark>)
~~~
legt die Geschwindigkeit der Turtle fest. Für `stufe` können folgende Werte eingesetzt werden: `"fastest"`, `"fast"`, `"normale"`, `"slow"`, `"slowest"`.

## Animation

~~~ python
tracer(0)
~~~
schaltet die Turtle-Animation aus. Du siehst danach nicht mehr, wie die Turtle zeichnet. Damit du das fertige Bild siehst, musst du am Ende des Programms `update()` aufrufen.

~~~ python
tracer(1)
~~~
schaltet die Turtle-Animation wieder ein.

~~~ python
update()
~~~
zeigt das gezeichnete Bild an, wenn die Turtle-Animation ausgeschaltet ist.

## Ereignisse

~~~ python
onkey(<mark>unterprogramm</mark>, <mark>taste</mark>)
~~~
teilt Python mit, dass das angegebene Unterprogramm aufgerufen werden soll, wenn die angegebene Taste gedrückt wird.

~~~ python
listen()
~~~
weist die Turtle an, auf Tastatureingaben zu achten.

## Eingabeboxen

Mit dem folgenden Befehl wird eine Eingabebox angezeigt, um eine Zahl einzulesen. Dem Befehl werden zwei Texte als Parameter übergeben: zuerst der Titel der Eingabebox und dann der Text:

``` python
x = numinput("Eingabe", "Gib eine Zahl ein:")
```

Mit dem folgenden Befehl wird eine Eingabebox angezeigt, um einen Text einzulesen:

``` python
x = textinput("Eingabe", "Gib deinen Namen ein:")
```

[4]: ?page=2-2-colour/
