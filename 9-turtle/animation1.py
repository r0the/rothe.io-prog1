from turtle import *

speed(0)
tracer(0)
x = 0
y = 0
vx = 0.5

def up():
    global y
    y = y + 5

onkey(up, "Up")


def ball():
    color("red")
    goto(x, y)
    dot(20)

def update():
    clear()
    ball()
    ontimer(update, 2)

listen()
ontimer(update, 2)
mainloop()
