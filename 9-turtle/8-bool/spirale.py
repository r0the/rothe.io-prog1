from turtle import *

w = 24
while w > 2:
    forward(6)
    left(w)
    w = w * 0.99
