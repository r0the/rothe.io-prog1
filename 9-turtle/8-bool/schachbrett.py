from turtle import *

speed(0)

feld = 20

def quadrat(seite):
    for i in range(4):
        forward(seite)
        left(90)

penup()
for y in range(8):
    for x in range(8):
        schwarz = (x + y) % 2 == 0
        if schwarz:
            fillcolor("black")
        else:
            fillcolor("light gray")
        begin_fill()
        quadrat(feld)
        end_fill()
        forward(feld)
    backward(8 * feld)
    left(90)
    forward(feld)
    right(90)
