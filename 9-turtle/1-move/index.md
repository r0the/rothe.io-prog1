# 9.1 Turtle bewegen
---

## Einführung

Programmieren heisst, einer Maschine Befehle zu erteilen und sie damit zu steuern. Die erste solche Maschine, die du steuerst, ist eine kleine Schildkröte auf dem Bildschirm: Die Turtle. Was kann diese Turtle und was musst du wissen, um sie zu steuern?

Turtlebefehle werden grundsätzlich Englisch geschrieben und enden immer mit einem Klammerpaar. Dieses enthält weitere Angaben zum Befehl. Selbst wenn keine weiteren Angaben nötig sind, muss ein leeres Klammerpaar vorhanden sein. Die Klein-/Grossschreibung muss exakt eingehalten werden.

Die Turtle kann sich innerhalb ihres Fensters bewegen und dabei eine Spur zeichnen. Bevor die Turtle aber loslegt, musst du den Computer anweisen, dir eine solche Turtle zu erzeugen. Das machst du mit dem Befehl `Turtle()`. Um die Turtle zu bewegen verwendest du die drei Befehle `forward(distanz)`, `left(winkel)` und `right (winkel)`.

Programmierkonzepte: Quellprogramm editieren, Programm ausführen, Programmsequenz

## Dein erstes Programm

So sieht dein erstes Programm mit der Turtle aus. Klicke auf das Kopieren-Icon oben rechts und füge es in den Editor ein. Führe es aus, indem du auf den grünen Start-Knopf klickst. Die Turtle zeichnet ein rechtwinkliges Dreieck.

Die Turtlebefehle sind in einer Datei (einem sogenannten Modul) `turtle` gespeichert.

Mit `import` sagst du dem Computer, dass er diese Befehle zur Verfügung stellen soll. In weiteren Zeilen stehen dann die Befehle (auch Anweisungen genannt) für die Turtle selber.

::: columns 2
``` python triangle.py
```
***
![](./triangle.png)
:::


::: info
Am Anfang jedes Turtle-Programms musst du zuerst das Turtle-Modul laden:

``` python
from turtle import *
```

Danach kannst du der Turtle beliebig viele Befehle geben. Die drei Befehle, die die Turtle sicher versteht sind:

~~~ python
forward(<mark>n</mark>)
~~~
bewegt die Turtle um `n` Pixel nach vorne.

~~~ python
left(<mark>a</mark>)
~~~
dreht die Turtle um `a` Grad nach links.

~~~ python
right(<mark>a</mark>)
~~~
dreht die Turtle um `a` Grad nach rechts.

:::


## Personalisierte Turtle

Du kannst mit dem Befehl `shape("turtle")` wird eine Schildkröte als Form für die festgelegt. Es können auch andere vordefinierte Formen gewählt werden.

Beache, dass die Bezeichnung der Form in Anführungszeichen stehen muss.

Im folgenden Programm zeichnet die Turtle ein Kreuz mit gefüllten Kreisen an den Endpunkten.

::: columns 2
``` python shape.py
```
***
![](./shape.png)
:::

::: info

~~~ python
shape(<mark>form</mark>)
~~~
ändert das Aussehen der Turtle. Für `form` können folgende Werte eingesetzt werden: - - `"arrow"`
- `"turtle"`
- `"circle"`
- `"square"`
- `"triangle"`
- `"classic"`

~~~ python
back(<mark>n</mark>)
~~~
bewegt die Turtle um `n` Pixel rückwärts.

~~~ python
dot(<mark>r</mark>)
~~~
zeichnet an der aktuellen Position der Turtle einen ausgefüllten Kreis mit einem Radius von `r` Pixel.
:::

::: exercise
#### :exercise: Aufgabe 1 – Quadrate
Zeichne mit der Turtle zwei Quadrate ineinander.

![](./exercise1.png)
***
``` python solutions/exercise1.py
```
:::

::: exercise
#### :exercise: Aufgabe 2 – Kreuz
Verwende zusätzlich den Befehl `dot()`, um folgende Figur zu zeichnen:

![](./exercise2.png)
***
``` python solutions/exercise2.py
```
:::

::: exercise
#### :exercise: Aufgabe 3 – Haus vom Nikolau
Das «Haus vom Nikolaus» ist ein Zeichenspiel für Kinder. Ziel ist es, das besagte Haus in einem Linienzug aus genau acht Strecken zu zeichnen, ohne dabei eine Strecke zweimal zu durchlaufen. Zeichne das Haus vom Nikolaus mithilfe der Turtle.

![](./exercise3.png)
***
``` python solutions/exercise3.py
```
:::

::: exercise
#### :extra: Aufgabe 4 – Turm
Zeichne das folgende Bild. Die Seitenlänge der Quadrate ist 100. Es ist gleichgültig, wo die Turtle mit der Zeichnung beginnt/endet.

![](./exercise4.png)
***
``` python solutions/exercise4.py
```
:::
