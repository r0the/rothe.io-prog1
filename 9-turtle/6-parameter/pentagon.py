from turtle import *

def pentagon(seite, farbe):
    pencolor(farbe)
    for i in range(5):
        forward(90)
        left(72)

pentagon(100, "red")
left(120)
pentagon(80, "green")
left(120)
pentagon(60, "violet")
