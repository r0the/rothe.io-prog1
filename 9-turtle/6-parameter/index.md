# 9.6 Parameter
---

Beim Befehl `forward()` gibst du in Klammern an, um welche Strecke die Turtle vorwärts gehen soll. Dieser Wert in den Klammern gibt an, wie weit vorwärts gegangen wird. Er präzisiert den Befehl und heisst ein **Parameter**: Hier ist es eine Zahl, die bei jeder Verwendung von `forward()` anders sein kann. Im vorhergehenden Kapitel hast du einen eigenen Befehl `quadrat()` definiert. Im Unterschied zu `forward()` ist die Seitenlänge dieses Quadrats aber immer 100 Pixel. Dabei wäre es doch in vielen Fällen praktisch, die Seitenlänge des Quadrats anpassen zu können. Wie geht das?

Programmierkonzepte: Parameter, Parameterübergabe

## Befehle mit Parameter

Auch in diesem Programm definieren wir ein Quadrat. An Stelle der leeren Parameterklammer bei der Definition der Funktion `square()`, setzen wir den Parameternamen `seite` ein und verwenden diesen beim Aufruf von `forward(seite)`.

Du kannst dadurch `square` mehrmals verwenden und bei jeder Verwendung eine Zahl für `seite` angeben.

Mit `square(80)` zeichnet die Turtle ein Quadrat mit der Seitenlänge von 80 Pixel, mit `square(50)` eines mit der Seitenlänge von 50 Pixel.

::: columns 2
``` python squares.py
```
***
![](./squares.png)
:::

::: info
**Parameter** sind Platzhalter für Werte, die bei jedem Aufruf eines Unterprogramms anders sein können. Du gibst den Parameter bei der Definition des Unterprogramms hinter den Namen in einem Klammerpaar an.

~~~ python
def befehlsname(parameter):
    Anweisungen, die
    parameter verwenden
~~~

Der Parametername ist frei wählbar, sollte aber seine Bedeutung wiederspiegeln. Bei der Verwendung des Befehls gibst du wieder in Klammern den Wert an, den der Parameter haben soll.

~~~ python
befehlsname(123)
~~~

Hier wird der Parameter im ganzen Befehl durch 123 ersetzt.
:::

## Mehrere Parameter

Befehle können mehrere Parameter besitzen. Beim Quadrat kannst du mit `def quadrat(seite, farbe)` als Parameter `seite` und `farbe` wählen.

Du kannst dann square viel flexibler verwenden.Mit `quadrat(100, "red")` zeichnet die Turtle ein rotes Quadrat mit der Seitenlänge von 100 Pixel, mit `quadrat(80, "green")` ein grünes mit der Seitenlänge von 80 Pixel.

::: columns 2
``` python squares2.py
```
***
![](./squares2.png)
:::

::: info
Befehle können mehrere Parameter besitzen. Diese werden in der Parameterklammer mit Komma getrennt angegeben.

~~~ python
def befehlsname(parameter1, parameter2, ...):
    Anweisungen, die parameter1
    und parameter2 verwenden
~~~

Die Reihenfolge der Parameter in der Parameterklammer bei der Definition des Befehls muss mit der Reihenfolge der Werte beim Aufruf des Befehls übereinstimmen.
:::

::: exercise
#### :exercise: Aufgabe 1 – Dreiecke
Definiere einen Befehl `dreieck(farbe)`, mit welchem die Turtle farbige Dreiecke zeichnen kann. Zeichne vier Dreiecke in den Farben Rot, Grün, Blau und Violett.

![](./exercise1.png)
***
``` python solutions/exercise1.py
```
:::

::: exercise
#### :exercise: Aufgabe 2 – Kreise
Definiere einen Befehl `kreis(radius, farbe)`, mit welchem die Turtle einen farbigen Kreis zeichnet. Du kannst dabei den vordefinierten Turtle-Befehl `circle(radius)` verwenden. Zeichne die nebenstehende Figur.

![](./exercise2.png)
***
``` python solutions/exercise2.py
```
:::

::: exercise
#### :exercise: Aufgabe 3 – Fünfecke
Das folgende Programm zeichnet leider 3 gleich grosse Fünfecke, aber nicht wie gewünscht verschieden grosse. Warum nicht? Korrigiere es.

``` python pentagon.py
```
***
``` python solutions/exercise3.py
```
:::

::: exercise
#### :exercise: Aufgabe 4 – Segmente
Du sagst der Turtle mit dem Befehl `segment()`, sich um eine bestimmte Strecke s vorwärts zu bewegen und  sich um einen bestimmten Winkel w nach rechts zu drehen:

``` python
def segment(s, w):
    forward(s)
    right(w)
```

Schreibe ein Programm, das diesen Befehl 92 mal mit s = 300 und w = 151 ausführt. Mit `setpos(x, y)` kannst du die Turtle zu Beginn geeignet im Fenster positionieren.
***
![](./solutions/exercise4.png)

``` python solutions/exercise4.py
```
:::

::: exercise
#### :extra: Zusatzaufgabe 5 – mehr Segmente
Die Turtle soll zwei, drei oder vier `segment`-Bewegungen ausführen. Schau dir die schönen Grafiken in folgenden Fällen an:

1. 37 Wiederholungen von zwei aufeinanderfolgenden Segmenten:

    - s = 77 und w = 140.86
    - s = 310 und w = 112

2. 46 Wiederholungen von drei aufeinanderfolgenden Segmenten:

    - s = 15.4 und w = 140.86
    - s = 62 und w = 112
    - s = 57.2 und w = 130

3. 68 Wiederholungen von vier aufeinanderfolgenden Segmenten:

    - s = 31 und w = 141
    - s = 112 und w = 87.19
    - s = 115.2 und w = 130
    - s = 186 und w = 121.43
:::
