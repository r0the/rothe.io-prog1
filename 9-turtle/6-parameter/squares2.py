from turtle import *

def quadrat(seite, farbe):
    pencolor(farbe)    
    for i in range(4):
        forward(seite) 
        left(90)
      
quadrat(100, "red")

left(120)
quadrat(80, "green")

left(120)
quadrat(60, "violet")