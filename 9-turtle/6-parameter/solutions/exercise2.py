from turtle import *

def kreis(radius, farbe):
    pencolor(farbe)
    circle(radius)

kreis(20, "red")
kreis(40, "green")
kreis(60, "blue")
kreis(80, "violet")

hideturtle()
