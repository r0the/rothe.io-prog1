from turtle import *

def segment(s, w):
    forward(s)
    right(w)

tracer(0)
for i in range(92):
    segment(300, 151)
hideturtle()
update()
