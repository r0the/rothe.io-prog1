from turtle import *

def dreieck(farbe):
    pencolor(farbe)
    for i in range(3):
        forward(100)
        left(120)

dreieck("red")
left(90)
dreieck("green")
left(90)
dreieck("blue")
left(90)
dreieck("violet")

hideturtle()