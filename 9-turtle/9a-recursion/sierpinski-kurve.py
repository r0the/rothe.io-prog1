from turtle import *

def seite(länge, n):
    if n > 0:
        länge = länge / 3
        seite(länge, n - 1)
        left(60)
        seite(länge, n - 1)
        right(120)
        seite(länge, n - 1)
        left(60)
        seite(länge, n - 1)
    else:
        forward(länge)


speed("fastest")
tracer(0)
hideturtle()

def dreieck(n):
    seite(400, n)
    right(120)
    seite(400, n)
    right(120)
    seite(400, n)
    right(120)

dreieck(8)
update()
