# Rekursion
---

Wir definieren ein Unterprogramm `seite`, welches entweder einen Zacken zeichnet oder eine gerade Strecke, abhängig davon, ob der Parameter `n` grösser als Null ist:

::: columns 2
``` python ./sierpinski-kurve1.py
```
***
![](./sierpinski-kurve1.png)
:::

Nun ändern wir das Unterprogramm `seite` ab. Wenn `n` grösser als Null ist, dann verwenden wir anstelle von `forward` das Unterprogramm selbst, um die vier kurzen Strecken zu zeichnen. Vorher ziehen wir aber 1 von `n` ab. Wir ersetzen also die Aufrufe von

``` python
forward(länge / 3)
```

durch

``` python
seite(länge / 3, n - 1)
```

Der Aufruf `seite(400, 1)` zeichnet immer noch dasselbe. Interessant wird es, wenn für `n` eine grössere Zahl angegeben wird, z.B. 2 oder 3.

::: columns 2
``` python ./sierpinski-kurve2.py
```
***
![](./sierpinski-kurve2.png)
:::

::: exercise
#### :exercise: Aufgabe 1 - Sierpinski-Schneeflocke

Erweitere das obenstehende Programm so, dass die untenstehende Figur gezeichnet wird. Definiere dazu ein Unterprogramm `dreieck(n)`, welches das Unterprogramm `seite` drei Mal aufruft.

![](./sierpinski-kurve.png)

**Tipp:** Wenn Du n > 5 wählst, dauert das Zeichnen sehr lange.

**Tipp:** Verwende die folgenden Befehle, um die Turtle schneller zeichnen zu lassen:

``` python
hideturtle() # Turtle nicht anzeigen, sieht schöner aus
tracer(0) # Animation ausschalten

# hier zeichnen
dreieck(5)

update() # gezeichnetes anzeigen
```
:::

::: exercise
#### :exercise: Aufgabe 2 - Schneeflocke

Versuche, mit der gleichen Methode die folgende Schneeflocke zu zeichnen:

![](./schneeflocke.png)
:::


::: exercise
#### :exercise: Aufgabe 3 - Sierpinski-Teppich

Versuche, mit der gleichen Methode die folgende Figur zu zeichnen:

![](./sierpinski-teppich.png)
:::
