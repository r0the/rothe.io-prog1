from turtle import *

def seite(länge, n):
    if n > 0:
        länge = länge / 3
        n = n - 1
        seite(länge, n)
        left(60)
        seite(länge, n)
        right(120)
        seite(länge, n)
        left(60)
        seite(länge, n)
    else:
        forward(länge)

seite(400, 2)
