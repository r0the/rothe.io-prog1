from turtle import *

def seite(länge, n):
    if n > 0:
        länge = länge / 3
        forward(länge)
        left(60)
        forward(länge)
        right(120)
        forward(länge)
        left(60)
        forward(länge)
    else:
        forward(länge)

seite(400, 1)
