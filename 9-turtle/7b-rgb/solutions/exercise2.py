from turtle import *

def winkelfarbe(h):
    v = h / 60
    if h < 60:
        return 1, v, 0
    elif h < 120:
        return 2 - v, 1, 0
    elif h < 180:
        return 0, 1, v - 2
    elif h < 240:
        return 0, 4 - v, 1
    elif h < 300:
        return v - 4, 0, 1
    else:
        return 1, 0, 6 - v

speed("fastest")
hideturtle()

def sektor(radius, winkel):
    begin_fill()
    forward(radius)
    left(90)
    circle(radius, winkel)
    left(90)
    forward(radius)
    left(180)
    end_fill()

radius = 150
for i in range(360):
    farbe = winkelfarbe(i)
    pencolor(farbe)
    fillcolor(farbe)
    sektor(radius, 1)

penup()
right(90)
forward(radius - 50)
left(90)
pendown()

speed("fastest")
color("white")
fillcolor("white")
begin_fill()
circle(radius - 50)
end_fill()
