from turtle import *

def winkelfarbe(h):
    v = h / 60
    if h < 60:
        return 1, v, 0
    elif h < 120:
        return 2 - v, 1, 0
    elif h < 180:
        return 0, 1, v - 2
    elif h < 240:
        return 0, 4 - v, 1
    elif h < 300:
        return v - 4, 0, 1
    else:
        return 1, 0, 6 - v

anzahl_farben = 7
dicke = 20

radius = 200
hue = 0

pensize(dicke)
penup()
for i in range(anzahl_farben):
    forward(radius)
    left(90)

    farbe = winkelfarbe(hue)
    pencolor(farbe)
    pendown()
    circle(radius, 180)
    penup()

    left(90)
    forward(radius)

    radius = radius - dicke
    hue += 360 / anzahl_farben
