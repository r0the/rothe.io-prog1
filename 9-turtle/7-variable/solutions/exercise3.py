from turtle import *

def quadrat(seite):
    for i in range(4):
        forward(seite)
        left(90)

seite = 8
for i in range(10):
    quadrat(seite)
    seite = seite + 10
