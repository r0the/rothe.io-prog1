from turtle import *
from tkinter.simpledialog import *

def neck(n):
    for i in range(n):
        forward(50)
        left(360 / n)

speed("fast")
n = askinteger("Eingabe", "Anzahl Ecken:")
neck(n)
