from turtle import *
from tkinter.simpledialog import *

def quadrat(seite):    
    for i in range(4):
        forward(seite) 
        left(90)

speed("fast")
s = askinteger("Eingabe", "Gib die Seitenlänge ein:")
quadrat(s)
