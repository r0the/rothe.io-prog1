import turtle

raph = turtle.Turtle()

raph.shape("turtle")
raph.pencolor("black")
raph.pensize("10")

raph.circle(90,360)

raph.pencolor("green")

raph.penup()
raph.right(90)
raph.forward(100)
raph.left(90)
raph.forward(100)
raph.pendown()

raph.circle(90,360)

raph.pencolor("gold")

raph.penup()
raph.left(180)
raph.forward(200)
raph.right(180)
raph.pendown()

raph.circle(90,360)

raph.pencolor("blue")

raph.penup()
raph.left(90)
raph.forward(100)
raph.left(90)
raph.forward(100)
raph.right(180)
raph.pendown()

raph.circle(90,360)

raph.pencolor("red")

raph.penup()
raph.forward(400)
raph.pendown()

raph.circle(90,360)
