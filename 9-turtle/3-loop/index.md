# 9.3 Wiederholung
---

## Einführung

Computer sind besonders gut darin, die gleichen Anweisungen (also auch Turtle-Befehle) immer wieder zu wiederholen. Um ein Quadrat zu zeichnen, musst du also nicht viermal die Befehle `forward(100)` und `left(90)` eingeben. Es genügt auch, der Turtle zu sagen, sie soll diese zwei Anweisungen viermal wiederholen.

Mit der Anweisung `for i in range(n)` sagst du der Turtle, sie soll einige Befehle eine bestimmte Anzahl Mal wiederholen. Damit der Computer weiss, dass diese Befehle zusammengehören (einen Programmblock bilden), müssen diese gleich weit eingerückt sein. Wir verwenden für Einrückungen grundsätzlich vier Leerschläge.

Programmierkonzepte: Einfache Wiederholstruktur statt Codeduplikation

## for-Struktur

Um ein Quadrat zu zeichnen, muss die Turtle vier mal geradeaus gehen und sich je um 90° drehen. Würdest du das alles untereinander schreiben, dann würde das Programm ziemlich lange.

Mit der Anweisung `for i in range(4)` sagst du der Turtle, sie soll die eingerückten Zeilen viermal wiederholen. Achtung: Vergiss den Doppelpunkt nicht!

::: columns 2
``` python square.py
```
***
![](./square.png)
:::

::: info
Mit `for i in range(Anzahl):` sagst du dem Computer, er soll eine oder mehrere Anweisungen «Anzahl» Mal wiederholen, bevor er weitere Anweisungen ausführt. Alles, was wiederholt werden soll, muss unter `for` stehen und eingerückt sein.

~~~ python
for i in range(Anzahl):
    Anweisungen, die
    wiederholt werden
    sollen
~~~
:::

## Töne Wiederholen

Ein typisches Beispiel für eine Wiederholung ist das Dah-Dih-Dah-Dih eines Feuerwehr-Autos. Mit dem Modul `musicalbeeps` kannst du eine solche Tonfolge leicht erzeugen und gleichzeitig spasseshalber die Turtle eine Zickzack-Kurve zeichnen lassen.

Dazu musst du zunächst das Modul `musicalbeeps` installieren und anschliessend in deinem Skript importieren:

``` python
from musicalbeeps import *
```

Dann muss der Musikplayer vorbereitet werden:

``` python
player = Player()
```

Nun können Töne abgespielt werden. Dabei steht `C5` für das C in der fünften Oktave.

``` python
player.play_note("C5", 0.4)
```

`0.4` ist die Dauer des Tons in Sekunden.

``` python sound.py
```

::: info

~~~ python
goto(<mark>x</mark>, <mark>y</mark>)
~~~
bewegt die Turtle zu den Koordinaten `x`, `y`.

:::

## Verschachtelte Schleifen

Ein Quadrat gelingt einfach mit einer vierfachen Wiederholung. Nun sollst du 20 Quadrate zeichnen und die Quadrate etwas gegeneinander verdrehen. Dazu musst du zwei `for`-Anweisungen ineinander schachteln. Im inneren Programmblock zeichnet die Turtle ein Quadrat und dreht anschliessend um 18 Grad nach rechts. Die äussere `for`-Anweisung wiederholt dies 20 mal. Beachte dabei die korrekte Einrückung bei den Anweisungen, die wiederholt werden sollen.

Mit `speed("fastest")` kann man die Geschwindigkeit der Turtle erhöhen, so wird die Zeichnung schneller fertig.

::: columns 2
``` python squares.py
```
***
![](./squares.png)
:::

::: info

`for`-Schleifen lassen sich verschachteln. Eine korrekte Einrückung bei den Anweisungen, die wiederholt werden sollen, ist wichtig.

~~~
speed(<mark>stufe</mark>)
~~~
legt die Geschwindigkeit der Turtle fest. Für `stufe` können folgende Werte angegeben werden:

- `"slowest"`
- `"slow"`
- `"normal"`
- `"fast"`
- `"fastest"`
:::

::: exercise
#### :exercise: Aufgabe 1 – Treppe
Zeichne eine Treppe mit 7 Stufen.

![](./exercise1.png)
***
``` python solutions/exercise1.py
```
:::

::: exercise
#### :exercise: Aufgabe 2 – Stern
Zeichne einen Stern. Verwende dabei den Befehl `back()`.

![](./exercise2.png)
***
``` python solutions/exercise2.py
```
:::

::: exercise
#### :exercise: Aufgabe 3 – ausgefüllter Stern
Einen «richtigen» Stern kannst du mit den Drehwinkeln 140° und 80° zeichnen.

![](./exercise3.png)
***
``` python solutions/exercise3.py
```
:::

::: exercise
#### :exercise: Aufgabe 4 – Quadrate
Zeichne folgende Figur mit zwei verschachtelten `for`-Anweisungen. Im inneren `for`-Block wird ein Quadrat gezeichnet.

![](./exercise4.png)
***
``` python solutions/exercise4.py
```
:::

::: exercise
#### :exercise: Aufgabe 5 – Perlenkette
Zeichne eine Perlenkette, die aus 18 Perlen (dots) besteht

![](./exercise5.png)
***
``` python solutions/exercise5.py
```
:::

::: exercise
#### :exercise: Aufgabe 6 – Vogel
Zeichne einen Vogel. Einen Bogen erhältst du, indem du die Befehle

```
forward(2)
right(3)
```

mehrmals wiederholst.

![](./exercise6.png)
***
``` python solutions/exercise6.py
```
:::
