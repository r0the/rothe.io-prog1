from musicalbeeps import *
from turtle import *

player = Player()

penup()
goto(-200, 0)
pendown()
left(45)

for i in range(5):
    player.play_note("C5", 0.4)   
    forward(50)
    right(90)
    player.play_note("C4", 0.4)   
    forward(50)  
    left(90)
