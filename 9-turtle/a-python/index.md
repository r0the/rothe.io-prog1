# :mdi-bookshelf: Sprachelemente
---

Du hast bis jetzt fünf Arten von Python-Befehlen kennengelernt. Alle Python-Programme werden aus diesen Befehlsarten zusammengesetzt:

## Import-Befehl

Um vordefinierte Python-Befehle aus einem Modul zu verwenden, müssen diese importiert werden. Dies geschieht mit dem Import-Befehl:

``` python
from turtle import *
```

## Unterprogramm aufrufen

Um ein vordefiniertes Unterprogramm aufzurufen wird der Name des Unterprogramms gefolgt von Klammern hingeschrieben. In den Klammern können **Parameter** stehen.
```
begin_fill()
stufe(50)
```

## Unterprogramm definieren

Ein Unterprogramm fasst mehrere Befehle zusammen, welche später mehrmals aufgerufen werden können. Unterschiedliche Werte für verschiedene Aufrufe des gleichen Unterprogramms können als **Parameter** übergeben werden.

``` python
def stufe(seite):
    forward(laenge)
    left(90)
    forward(laenge)
    right(90)
```

## Zuweisung

Mit einer Zuweisung wird ein Wert in einer Variable gespeichert. Mit einer erneuten Zuweisung kann ein neuer Wert in der Variable gespeichert werden.

``` python
a = 2
a = 3
a = a + 5
```

## Grundrechenoperationen

Die Grundrechenoperationen werden in Python folgendermassen geschrieben:

| Operation      | mathematisch | Python   |
|:-------------- |:------------ |:-------- |
| Addition       | $a + b$      | `a + b`  |
| Subtraktion    | $a - b$      | `a - b`  |
| Multiplikation | $a \cdot b$  | `a * b`  |
| Division       | $a ÷ b$      | `a / b`  |
| Potenz         | $a^b$        | `a ** b` |


## `for`-Schleife

Mit einer Schleife können Befehle eine bestimmte Anzahl Mal wiederholt werden.

``` python
for i in range(5):
    forward(100)
    left(90)
```

## Wahrheitswerte

Die Wahrheitswerte `True` und `False` ergeben sich aus **Bedingungen**, z.B. aus einem Vergleich:

| Name             | Mathematisch | Python |
| ---------------- |:------------:|:------:|
| *grösser*        |    $\lt$     |  `<`   |
| *kleiner*        |    $\gt$     |  `>`   |
| *grösser gleich* |    $\leq$    |  `<=`  |
| *kleiner gleich* |    $\geq$    |  `>=`  |
| *gleich*         |     $=$      |  `==`  |
| *ungleich*       |    $\neq$    |  `!=`  |

## `while`-Schleife

Mit eine `while`-Schleife werden Befehle so lange wiederholt, wie eine Bedingung erfüllt ist.

``` python
w = 0
while w < 20:
    w = w * 1.1
```

## Verzweigung

Mit eine Verzweigung werden Befehle nur ausgeführt, wenn eine bestimmte Bedingung zutrifft:

``` python
if i == 2:
    pencolor("red")
else:
    pencolor("blue")
```
