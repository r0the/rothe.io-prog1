# 8 Algorithmen
---

In diesem Kapitel wird erklärt, was Algorithmen sind. Im ersten Abschnitt werden die wichtigsten Begriffe eingeführt. Es folgen Beispiele von Algorithmen, an welchen die Begriffe illustriert werden.

Um sich in das Thema einzuarbeiten, empfiehlt es sich, zuerst anhand der Gruppenarbeit die Beispiele kennenzulernen und erst dann die Theorie zu lesen.

* [8.1 Algorithmus](?page=1-algo/1-algorithm/)
* [8.2 Faltanleitung](?page=1-algo/2-origami/)
* [8.3 Notenblatt](?page=1-algo/3-sheet-music/)
* [8.4 Rezept](?page=1-algo/4-recipe/)
* [8.5 Flussdiagramm](?page=1-algo/5-flowchart/)
