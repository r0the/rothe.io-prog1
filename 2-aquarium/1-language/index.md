 # Sprachelemente
 ---

::: warning
TODO:
- `import pgzrun` und `pgzrun.go()` erklären.
- Flussdiagramm pgzero überarbeiten.
:::

::: exercise
#### :exercise: Aufgabe 1.1

Kopiere das folgende Python-Programm nach Thonny und starte es:

``` python ./aufgabe1_1.py
```
:::

Im folgenden werden die Elemente des Beispielprogramms erklärt.

## Bibliotheken verwenden

Beim Programmieren schreiben entwickeln wir nicht die ganze Funktionalität von Grund auf selbst, sondern wir verwenden Bausteine, welche von anderen Programmieren entwickelt worden sind. Diese Bausteine werden in Form von **Bibliotheken** zu Verfügung gestellt. Damit wir einen Baustein verwenden können, müssen wir:

1. das entsprechende [Paket installieren](?page=/ref/thonny/3-packages/).
2. in unserem Programm die Bibliothek importieren mit einer `import`-Anweisung.

## Name

Das erste Wort auf Zeile 3 im Programm ist `WIDTH`. Es ist der Name einer Variable. In Python besteht ein Name aus einem Wort. Mehrere Wörter können durch einen Unterstrich `_` zu einem Namen zusammengesetzt werden: `hintergrundfarbe`.

Dabei ist die Gross- und Kleinschreibung wichtig: `width`, `WIDTH` und `Width` sind drei unterschiedliche Variablen.

Namen dürfen grundsätzlich frei gewählt werden. Es gibt aber einige Namen, welche in Python eine  spezielle Bedeutung haben und nicht verwendet werden dürfen. Ausserdem sind manche Namen durch Pygame Zero vorgegeben. Beispielsweise wird die Breite des Fensters die Variable `WIDTH` festgelegt. Natürlich darf auch die Variable `BREITE` verwendet werden, sie hat aber keine Auswirkung auf Pygame Zero.

## Zuweisung

Die erste Zeile ist eine **Zuweisung**. Sie besteht aus einem **Variablennamen**, einem Gleichheitszeichen `=` und einem **Wert**. Die Zuweisung

``` python
WIDTH = 800
```
weist der Variable mit dem Namen `WIDTH` den Wert `800` zu. Die Zuweisung darf nicht mit einer mathematischen Gleichheit verwechselt werden. Es ist treffender, sich eine Bewegung von rechts nach links vorzustellen. Der Wert rechts des Gleichheitszeichens wird in der Variable links davon gespeichert:

![](./assignment.svg)

::: exercise
#### :exercise: Experiment 1.2
Schreibe mehrere Zuweisungen für die Variable `WIDTH` mit unterschiedlichen Werten. Was geschieht?
:::

## Wert

In einer Variable wird immer ein Wert gespeichert. Python kennt verschiedene Arten von Werten. Zwei davon lernen wir hier kennen.

### Ganze Zahlen (int)

Im obenstehenden Programm werden einerseits die **ganzen Zahlen** `800` und `600` verwendet. Beim Programmieren verwendet man dafür normalerweise den englischen Begriff *integer*, welcher mit `int` abgekürzt wird.

### Tupel

Auf der dritten Zeile stehen drei kommagetrennte Zahlen: `0, 120, 200`. Diese bilden ein **Tupel**. Tupel werden verwendet um beispielsweise die RGB-Werte von Farben oder Koordinaten anzugeben.

::: exercise
#### :exercise: Experiment 1.3
Ändere das Tupel, welches der Variable `hintergrundfarbe` zugewiesen wird. Was geschieht?
:::

## Anweisung

Anweisungen sind die «Sätze» eines Programms oder Skripts. In Python steht jede Anweisung auf einer separaten Zeile. Es gibt verschiedene Arten von Anweisungen. Auf den drei ersten Zeilen steht je eine Zuweisung.

Eine Anweisung entspricht einer Aktion in einem Algorithmus. Anweisungen werden in Python immer der Reihe nach von oben nach unten abgearbeitet.

![](./flowchart-exercise-3a.svg)

::: exercise
#### :exercise: Experiment 1.4
Ändere die Reihenfolge der Anweisungen im Programm. Was geschieht?
:::

## Unterprogramm

Mit `def` Beginnt die Definition eines Unterprogramms. Anschliessend folgt der Name des Unterprogramms, ein Paar Klammern und ein Doppelpunkt.

Nun folgen eine oder mehrere Zeilen, welche um vier Leerzeichen eingerückt sind. Diese enthalten Anweisungen, welche aber nicht sofort ausgeführt werden. Alle eingerückten Anweisungen gehören zum Unterprogramm und werden erst ausgeführt, wenn das Unterprogramm *aufgerufen* wird.

``` python
def draw():
    screen.fill(hintergrundfarbe)
```

Die `def`-Zeile und alle folgenden eingerückten Zeilen gehören zur Definition des Unterprogramms und bilden eine einzige Anweisung.

**Wichtig:** die eingerückten Anweisungen werden bei der Definition des Unterprogramms **nicht** ausgeführt.

## Funktionsweise von Pygame Zero

Das Hauptprogramm von Pygame Zero wird erst gestartet, nachdem das ganze Skript **aquarium.py** abgearbeitet worden ist. Dafür sorgt der spezielle Modus des Mu-Editors.

Pygame Zero erstellt zunächst ein Fenster. Dazu werden die aktuellen Angaben in den Variablen `WIDTH` und `HEIGHT` verwendet. Anschliessend ruft Pygame Zero das Unterprogramm `draw()` auf, welches in **aquarium.py** definiert worden ist.

![](./flowchart-exercise-3b.svg)

## Hintergrund füllen

Erst beim Aufruf des Unterprogramms `draw()` wird die folgende Anweisung ausgeführt:

``` python
screen.fill(hintergrundfarbe)
```

Hier ist `screen` eine Variable, welche von Pygame Zero definiert worden ist. Sie enthält als Wert ein **Objekt**, welches das Fenster repräsentiert. `.fill()` ist ein Unterprogramm von diesem Objekt, welches das Fenster mit der angegebenen Farbe füllt.

Der Punkt `.` ist eine grundlegende Operation in Python. Er bedeutet, dass auf ein Element (hier ein Unterprogramm) eines Objekts zugegriffen wird.
