# Kollision und Leben
---

## Kollision entdecken

~~~ python
actor.colliderect(actor)
~~~
überprüft, ob sich zwei Aktoren berühren.

Diese Abfrage kann als Bedingung für eine Bedingte Ausführung verwendet werden:

``` python
if red_fish.colliderect(blue_fish):
    ...
```

## Punkte zählen

Um Punkte oder Anzahl Leben zu zählen, definieren wir eine Variable beim von uns gesteuerten Fisch:

``` python
red_fish.leben = 10
```

Jedes Mal, wenn «unser» Fisch einen anderen Fisch berührt, ziehen wir ein Punkt ab:

``` python
if red_fish.colliderect(blue_fish):
    red_fish.leben = red_fish.leben - 1
```


::: exercise
#### :exercise: Aufgabe 8 – Aquarium-Spiel

![](./images/fish_red_right.png) ![](./images/fish_bad_right.png) ![](./images/fish_zombie_right.png) ![](./images/heart.png) ![](./images/star.png)

Erweitere den Aquarium-Programm zu einem kleinen Spiel:

1. Mehrere «böse» Fische sollen selbständig durchs Aquarium schwimmen und an den Rändern jeweils umkehren.
2. Der «gute» Fisch wird mit der Tastatur gesteuert.
3. Der «gute» erhält eine Anzahl Leben, am Anfang 10:
4. Falls der «gute» Fisch mit dem «Bösen» kollidiert, verliert er ein Leben.
5. Wenn er ein Leben verloren hat, wird der «gute» Fisch zufällig neu platziert.
:::
