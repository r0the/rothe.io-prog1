WIDTH = 800
HEIGHT = 600
background_colour = 0, 120, 200
mein_fisch = Actor("fish_red_right")
mein_fisch.x = WIDTH / 2
mein_fisch.y = HEIGHT / 2
mein_fisch.leben = 10

boeser_fisch = Actor("fish_bad_right")
boeser_fisch.vx = 2
boeser_fisch.vy = 1

herz = Actor("heart")


def bewege_tastatur(actor):
    if keyboard.a and actor.left > 0:
        actor.x = actor.x - 2
    if keyboard.d and actor.right < WIDTH:
        actor.x = actor.x + 2
    if keyboard.w and actor.top > 0:
        actor.y = actor.y - 2
    if keyboard.s and actor.bottom < HEIGHT:
        actor.y = actor.y + 2


def bewege_automatisch(actor):
    actor.x = actor.x + actor.vx
    actor.y = actor.y + actor.vy
    if actor.right >= WIDTH:
        actor.vx = -actor.vx
    if actor.left < 0:
        actor.vx = -actor.vx
    if actor.bottom >= HEIGHT:
        actor.vy = -actor.vy
    if actor.top < 0:
        actor.vy = -actor.vy


def update():
    bewege_tastatur(mein_fisch)
    bewege_automatisch(boeser_fisch)
    if mein_fisch.colliderect(boeser_fisch):
        mein_fisch.leben = mein_fisch.leben - 1
        mein_fisch.x = WIDTH / 2
        mein_fisch.y = HEIGHT / 2


def zeichne_leben():
    herz.top = 10
    herz.left = 10
    for i in range(mein_fisch.leben):
        herz.draw()
        herz.x = herz.x + 50


def draw():
    screen.fill(background_colour)
    mein_fisch.draw()
    boeser_fisch.draw()
    zeichne_leben()
