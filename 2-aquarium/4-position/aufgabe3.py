import pgzrun

WIDTH = 800
HEIGHT = 600
background_colour = 0, 120, 200
red_fish = Actor("fish_red_right")
red_fish.x = WIDTH / 2
red_fish.top = 0

blue_fish = Actor("fish_blue_right")
blue_fish.right = WIDTH
blue_fish.y = HEIGHT / 2

yellow_fish = Actor("fish_yellow_right")
yellow_fish.right = blue_fish.left
yellow_fish.y = blue_fish.y

green_fish = Actor("fish_green_right")
green_fish.x = WIDTH / 2
green_fish.y = HEIGHT * 0.75

def draw():
    screen.fill(background_colour)
    red_fish.draw()
    blue_fish.draw()
    yellow_fish.draw()
    green_fish.draw()

pgzrun.go()
