# :extra: Automatische Bewegung
---

Nun möchten wir Fische programmieren, welche selbständig herumschwimmen und an den Rändern abprallen. Diese Fische müssen sich an etwas erinnern, nämlich ihre aktuelle Bewegungsrichtung und Geschwindigkeit.

Dazu legen wir im Aktor-Objekt zwei neue Variablen an, `vx` und `vy`:

``` python
blue_fish = Actor("blue_fish_right")
blue_fish.vx = 2
blue_fish.vy = 1
```

Bei der Änderung der Position verwenden wir `vx` und `vy` anstelle von fixen Werten:

``` python
blue_fish.x = blue_fish.x + blue_fish.vx
blue_fish.y = blue_fish.y + blue_fish.vy
```

So kann die Bewegungsrichtung des Fisch durch das Ändern von `vx` und `vy` angepasst werden. Beispielsweise kann der Fisch so am rechten Rand abprallen:

``` python
if blue_fish.x >= WIDTH:
    blue_fish.vx = -blue_fish.vx
```

Ein vollständiges Beispiel sieht so aus:

``` python
blue_fish = Actor("blue_fish_right")
blue_fish.vx = 2

def update():
    blue_fish.x = blue_fish.x + blue_fish.vx
    blue_fish.y = blue_fish.y + blue_fish.vy
    if blue_fish.x > WIDTH:
        blue_fish.vx = -blue_fish.vx

def draw():
    blue_fish.draw()
```

::: exercise
#### :exercise: Aufgabe 6
Erweitere das Programm **aquarium.py** so, dass sich ein oder zwei Fische wie oben beschrieben automatisch bewegen.
***
``` python ./aufgabe6.py
```
:::
