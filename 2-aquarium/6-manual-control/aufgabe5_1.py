import pgzrun

WIDTH = 800
HEIGHT = 600
background_colour = 0, 120, 200
red_fish = Actor("fish_red_right")
red_fish.x = WIDTH / 2
red_fish.y = HEIGHT / 2


def update():
    if keyboard.a:
        red_fish.x = red_fish.x - 2
    if keyboard.d:
        red_fish.x = red_fish.x + 2
    if keyboard.w:
        red_fish.y = red_fish.y - 2
    if keyboard.s:
        red_fish.y = red_fish.y + 2


def draw():
    screen.fill(background_colour)
    red_fish.draw()

pgzrun.go()
