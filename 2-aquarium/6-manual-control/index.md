# Manuelle Steuerung
---

Als nächstes wollen wir anschauen, wie wir einen Fisch mit der Tastatur steuern können. Dazu benötigen wir zwei Dinge:

1. Wir müssen überprüfen können, ob eine bestimmte Taste gedrückt ist.
2. Wir müssen eine Anweisung (das Bewegen) nur unter einer **bestimmten Bedingung** ausführen können.

Das folgende Flussdiagramm illustriert, was wir erreichen möchten. Die x-Koordinaten eines Aktors wird nur dann erhöht, wenn die Taste [D] gedrückt ist:

![](./flowchart-keyboard.svg)

## Tastatur

Pygame Zero definiert die Variable `keyboard`. Sie enthält ein Objekt, welches den Zustand der Tastatur wiedergibt. Für (fast) jede Taste besitzt das `keyboard`-Objekt eine Variable, welche Auskunft gibt, ob die Taste gerade gedrückt wird oder nicht.

Eine vollständige Liste aller Tasten findet sich in der [Pygame Zero-Referenz][1]. Für die Steuerung benötigen wir die folgenden vier Tasten:

| Taste | Variable     | Taste | Variable     |
| -----:|:------------ | -----:|:------------ |
|   [W] | `keyboard.w` |   [S] | `keyboard.s` |
|   [A] | `keyboard.a` |   [D] | `keyboard.d` |

## Bedingte Ausführung

Bei einer bedingten Ausführung werden eine oder mehrere Anweisungen nur ausgeführt, wenn eine Bedingung zutrifft:

``` python
if keyboard.d:
    red_fish.x = red_fish.x + 2
```

Dabei sind folgende Regeln zu beachten:

- Die Anweisung beginnt mit `if`.
- Es folgt eine **Bedingung**. Zur Zeit ist das eine Variable des `keyboard`-Objekts.
- Die Zeile muss mit einem Doppelpunkt `:` abgeschlossen werden.
- Die Anweisung, welche nur bedingt ausgeführt werden soll, muss um vier Leerzeichen eingerückt werden.

::: exercise
#### :exercise: Aufgabe 5.1 – Steuerung

Erweitere das Programm **aquarium.py** so, dass ein Fisch mit den Tasten [W], [A], [S] und [D] gesteuert werden kann. Der Fisch soll in der Mitte des Aquariums starten.

***
**Tipp:** Füge das obenstehende Beispiel einer bedingten Ausführung in das Unterprogramm `update` ein.

***
``` python ./aufgabe5_1.py
```
:::


## Vergleiche

Nun möchten wir noch verhindern, dass unser Fisch aus dem Aquarium herausschwimmen kann. Dazu benötigen wir eine zusätzliche Möglichkeit, eine Bedingung für die Bedingte Ausführung zu formulieren.

Ein Vergleich zweier Werte ist in Python ebenfalls eine Bedingung. In Python können Werte auf folgende Arten verglichen werden:

| Vergleich               | Mathematisch | Python   |
|:----------------------- |:------------ |:-------- |
| kleiner als             | $a < b$      | `a < b`  |
| kleiner als oder gleich | $a \leq b$   | `a <= b` |
| grösser als             | $a > b$      | `a > b`  |
| grösser als oder gleich | $a \geq b$   | `a >= b` |
| gleich                  | $a = b$      | `a == b` |
| nicht gleich            | $a \neq b$   | `a != b` |

::: warning Gleichheit und Zuweisung
Die Gleichheit wird durch zwei aufeinanderfolgende Gleichheitszeichen `==` dargestellt. Ein einzelnes Gleichheitszeichen `=` wird für die Zuweisung eines Werts an eine Variable verwendet.
:::

Damit kann beispielsweise folgende Bedingte Ausführung formuliert werden, welche den Aktor `red_fish` zurückholt, wenn er über den rechten Fensterrand hinausgeschwommen ist:

``` python
if red_fish.right > WIDTH:
    red_fish.right = WIDTH
```

Bedingungen können auch mit `and` und `or` kombiniert werden. Mit folgendem Code wird verhindert,    dass sich der Aktor `red_fish` überhaupt über den rechten Fensterrand hinausbewegen kann:

``` python
if keyboard.d and red_fish.right < WIDTH:
    red_fish.x = red_fish.x + 2
```

::: exercise
#### :exercise: Aufgabe 5.2 – Bleib im Aquarium
Erweitere das Programm **aquarium.py** so, dass sich der gesteuerte Fisch nicht über den Fensterrand hinausbewegen kann.
***
``` python ./aufgabe5_2.py
```
:::


[1]: ?page=0-4-keyboard
