# Bewegung
---

Um in Pygame Zero eine Animation zu erstellen, muss ein zusätzliches Unterprogramm mit dem Namen `update` definiert werden:

Wenn ein Unterprogramm mit dem Namen `update` vorhanden ist, ändert Pygame Zero sein Verhalten. Nun wird erst `update` aufgerufen und anschliessend `draw`. Dies wird so lange wiederholt, bis das Programm abgebrochen wird.

![Verhalten von Pygame Zero bei einer Animation](./flowchart-animation.svg)

Das entsprechende Python-Programm hat folgende Struktur:

``` python ./animation_template.py
```

::: exercise
#### :exercise: Aufgabe 4 – Animation

Erweitere das Programm **aquarium.py** so, dass sich die Fische bewegen.

***
Hier ist ein Beispiel `update`-Unterprogramm, welches den Aktor `red_fish` nach rechts bewegt:
``` python
def update():
    red_fish.x = red_fish.x + 2
```

***
``` python ./aufgabe4.py
```
:::
