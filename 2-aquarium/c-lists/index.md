# Listen
---

Im folgenden Bild siehst du eine Liste, die aus lauter Zahlen besteht. Darunter in roter Schrift wird die Position - meist **Index** genannt - der Elemente angezeigt. Wie immer in der Informatik wird bei Null mit Zählen begonnen.

![](./list-with-index.png)


## Listen erstellen
Im Zusammenhang mit Listen werden stets eckige Klammern `[` und `]` verwendet (sie erinnern an die in der Darstellung oben verwendeten Rahmen). Listen werden ebenfalls in einer Variable gespeichert, wie folgende Beispiele zeigen:

``` python
a = []                        # definiert eine leere Liste 'a'
b = [17, -2, 42, 107, 3, 0]   # definiert eine Liste 'b' mit 6 Elementen
```

## Listenelemente verwenden
Will man auf ein bestimmtes Element der Liste hinweisen, so verwendet man erneut die Schreibweise mit den eckigen Klammern und gibt darin den Index des gewünschten Elements an:

``` python
c[0] = 51                     # Wert des Elements bei Index 0 in 'c' ersetzen
c[1] = b[0] + b[1]            # Summe der ersten beiden Elemente speichern
```

## Funktionen für Listen
Im Zusammenhang mit Listen sind die folgenden Funktion sehr hilfreich:

``` python
anzahl = len(b)               # len() berechnet die Länge einer Liste
b.append(1029)                # append() hängt Elemente hinten an
b.insert(1, 555)              # das Element 555 wird bei Index 1 eingefügt
```

## Liste mit einer Schleife füllen

Der Index wird meist in der Variablen `i` gespeichert. Eine Liste der Quadratzahlen von 1 bis 10 erstellen:

``` python
list = []
for i in range(1, 11):
    list.append(i ** 2)       # i ** 2 bedeutet: i hoch 2
```


## Listenelemente in einer Schleife verwenden

Braucht man nur die einzelnen Elemente einer Liste aber nicht deren Index, so kann die folgende `for`-Schleife verwendet werden:
~~~ python
for element in list:
    Anweisung A
    Anweisung B
~~~
Dabei wird in der Variable `element` der erste Wert aus der Liste `list` gespeichert und anschliessend die eingerückten Anweisungen ausgeführt. Anschliessend wird das ganze für das zweite, das dritte usw. Element der Liste wiederholt.

Im folgenden Beispiel wird die Summe aller Elemente einer Liste ermittelt:

``` python
list = [1, 2, 4, 8, 16]
sum = 0
for element in list:
    sum = sum + element
```

## Liste von Aktoren

Das folgende Beispiel zeigt, wie eine grosse Menge von Aktoren einfach mit einer Liste verwaltet werden können:

``` python ./actor_list.py
```
