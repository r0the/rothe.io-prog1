# Verhalten verallgemeinern
---

Wenn wir mehrere sich bewegende Fische im Aquarium darstellen wollen, muss für jeden Fisch der Code für die Bewegung und das «Abprallen» am Rand ausgeführt werden. Das Kopieren des Codes für jeden Fisch ist umständlich, weil

- das Programm schnell unübersichtlich wird,
- die Actor-Variable jedesmall umbenannt werden muss und
- eine Änderung im Code für die Bewegung in jeder Kopie vorgenommen werden muss.

Eine elegante Alternative ist, den Code für die Bewegung in einem eigenen Unterprogramm zusammenzufassen. Da wird den Code für verschiedene Aktoren ausführen wollen, verwenden wir `actor` als Parameter.

Das folgende Unterprogramm definiert eine Bewegung für einen beliebigen Aktor:

``` python
def bewege_automatisch(actor):
    actor.x = actor.x + actor.vx
    actor.y = actor.y + actor.vy
    if actor.right >= WIDTH:
        actor.vx = -actor.vx
    if actor.left < 0:
        actor.vx = -actor.vx
    if actor.bottom >= HEIGHT:
        actor.vy = -actor.vy
    if actor.top < 0:
        actor.vy = -actor.vy
```

Nun kann das Unterprogramm verwendet werden, um mehrere Aktoren zu bewegen. Voraussetzung ist, das für jeden Aktor die Variablen `vx` und `vy` definiert werden:

``` python
fisch_a = Actor("fish_red_right")
fisch_a.vx = 2
fisch_a.vy = 1
fisch_b = Actor("fish_blue_right")
fisch_b.vx = 2
fisch_b.vy = 1

def update():
    bewege_automatisch(fisch_a)
    bewege_automatisch(fisch_b)
```

::: exercise
#### :exercise: Aufgabe 8
1. Fasse in deinem Aquarium-Programm die automatische sowie die manuelle Bewegung in je einem Unterprogramm zusammen.
2. Verwende die Unterprogramme, um mindestens einen Fisch manuell und zwei automatisch zu bewegen.
:::
