# Lebensanzeige
---

Um die Anzahl verbleibender Leben darzustellen, muss ein Herz mehrmals gezeichnet werden. Zuerst wird ein `Actor` für die Herz-Grafik erzeugt:

``` python
herz = Actor("heart")
```

Dazu definieren wir das Unterprogramm `zeichne_leben(n)`, welche das Herz `n` mal zeichnen soll. Zuerst wird das Herz in die obere linke Ecke gesetzt. Danach wird das Herz in einer Schleife gezeichnet und anschliessend nach rechts verschoben:

``` python
def zeichne_leben(n):
    herz.top = 10
    herz.left = 10
    for i in range(n):
        herz.draw()
        herz.x = herz.x + 50
```

Das Unterprogramm `zeichne_leben` muss im Unterprogramm `draw()` aufgerufen werden. Ein vollständiges Beispiel sieht so aus:

``` python ./draw_life.py
```

::: exercise
#### :exercise: Aufgabe 9
Erweitere dein Aquarium-Programm so, dass die Anzahl verbleibender Leben mit Herzen angezeigt werden.

![](./life.png)

:::
