from tkinter import *
from configurations import *

class View:
    def __init__(self):
        pass

    def create_board(self):
        self.create_canvas()
    
    def create_canvas(self):
        w = NUMBER_OF_COLUMNS * DIMENSION_OF_EACH_SQUARE
        h = NUMBER_OF_ROWS * DIMENSION_OF_EACH_SQUARE
        self.canvas = Canvas(self.parent, width=w, height=h)
        self.canvas.pack(padx=8, pady=8)
