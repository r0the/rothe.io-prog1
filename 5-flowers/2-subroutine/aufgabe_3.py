import pgzrun

TITLE = "Blumenwiese"
WIDTH = 1000
HEIGHT = 733

wiese = Actor("wiese")
veilchen = Actor("veilchen")
loewenzahn = Actor("loewenzahn")
orchidee = Actor("orchidee")

def zeichne_blume(blume, x, y):
    blume.x = x
    blume.y = y
    blume.draw()


def draw():
    wiese.draw()
    zeichne_blume(veilchen, 400, 600)
    zeichne_blume(loewenzahn, 500, 600)
    zeichne_blume(orchidee, 600, 600)

import pgzrun.go()
