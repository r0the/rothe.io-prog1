# Unterprogramm
---

Mit einem Unterprogramm können mehrere Befehle zusammengefasst werden. Um das Veilchen zu positionieren und zu zeichnen, können wir das folgende Unterprogramm definieren:

``` python
def zeichne_veilchen():
    veilchen.x = 400
    veilchen.y = 600
    veilchen.draw()
```

Im Unterprogramm `draw` wird nun das Unterprogramm `zeichne_veilchen` so aufgerufen:

``` python
def draw():
    zeichne_veilchen()
```

## Parameter

Möglicherweise möchten wir das Veilchen an unterschiedlichen horizontalen Positionen zeichnen. Dazu fügen wir dem Unterprogramm `veilchen` den Parameter `x` hinzu. Im Unterprogramm verwenden die die Parameter als Platzhalter für die Position:

``` python
def zeichne_veilchen(x):
    veilchen.x = x
    veilchen.y = 600
    veilchen.draw()
```

Bei jedem Aufruf des Unterprogramms `zeichne_veilchen` wird der gewünschte Wert für den Parameter angegeben:

``` python
def draw():
    zeichne_veilchen(400)
    zeichne_veilchen(500)
```

## Aktor als Parameter

Wenn wir verschiedene Blumen positionieren und zeichnen wollen, können wir die Blume als weiteren Parameter hinzufügen. Nun heisst das Unterprogramm `zeichne_blume`, da ja verschiedene Blumen gezeichnet werden:

``` python
def zeichne_blume(blume, x):
    blume.x = x
    blume.y = 600
    blume.draw()
```

Beim Aufruf des Unterprogramms `zeichne_veilchen` werden die gewünschten Werte für die Parameter angegeben:

``` python
def draw():
    zeichne_blume(veilchen, 400)
    zeichne_blume(orchidee, 500)
```

::: exercise Aufgabe 3 – Unterprogramm
1. Übernimm das Unterprogramm `zeichne_blume` in den Programm.
2. Ersetze im Unterprogramm `draw` die Zeichenbefehle für die Blumen durch Aufrufe von `zeichne_blume`.
3. Erweitere das Unterprogramm `zeichne_blume` so, dass auch die vertikale Position der Blume bei jedem Aufruf festgelegt werden kann.
***
``` python ./aufgabe_3.py
```
:::
