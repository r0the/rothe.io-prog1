# Aktoren
---

## Befehle

Die folgenden Befehle solltest du kennen:

~~~ python
veilchen = Actor("veilchen")
~~~
erzeugt einen neuen Aktor und ordnet ihn der Variable `veilchen` zu. In den Klammern wird der Name der Bilddatei ohne Dateiendung und in Anführungszeichen angegeben, also `"veilchen"`.

~~~ python
veilchen.x = 400
~~~
setzt für den Aktor `veilchen` x-Koordinate des Mittelpunkts auf den Wert `400`.

~~~ python
veilchen.y = 600
~~~
setzt für den Aktor `veilchen` y-Koordinate des Mittelpunkts auf den Wert `600`.

~~~ python
veilchen.draw()
~~~
zeichnet den Aktor `veilchen` an seiner aktuellen Position.

::: exercise Aufgabe 2 – Mehr Blumen
1. Erstelle analog zu `veilchen` je einen Aktor `loewenzahn` und `orchidee`.
2. Zeichne den Löwenzahn und die Orchidee rechts neben dem Veilchen.

![](./aufgabe_2.png)
***
``` python ./aufgabe_2.py
```
:::
