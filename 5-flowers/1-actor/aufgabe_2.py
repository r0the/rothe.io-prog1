import pgzrun

TITLE = "Blumenwiese"
WIDTH = 1000
HEIGHT = 733

wiese = Actor("wiese")
veilchen = Actor("veilchen")
loewenzahn = Actor("loewenzahn")
orchidee = Actor("orchidee")


def draw():
    wiese.draw()

    veilchen.x = 400
    veilchen.y = 600
    veilchen.draw()

    loewenzahn.x = 500
    loewenzahn.y = 600
    loewenzahn.draw()

    orchidee.x = 600
    orchidee.y = 600
    orchidee.draw()

pgzrun.go()
