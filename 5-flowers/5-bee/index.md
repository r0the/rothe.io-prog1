# Biene
---

## Animation

Um in Pygame Zero eine Animation zu programmieren, muss das Unterprogramm `update` definiert werden. Dort kann die Position von Aktoren verändert werden:

``` python
def update():
    biene.x = biene.x + 2
```

## Bedingte Ausführung

Bei einer bedingten Ausführung werden eine oder mehrere Anweisungen nur ausgeführt, wenn eine Bedingung zutrifft:

``` python
def update():
    if keyboard.d:
        biene.x = biene.x + 2
```

## Ändern des Aussehens

Das Aussehen eines Aktors kann folgendermassen geändert werden:

``` python
biene.image = "biene_links"
```

::: exercise Aufgabe 6 – Biene
1. Erstelle einen Aktor `biene` mit dem Bild `biene_rechts`.
2. Erweitere das Unterprogramm `draw` so, dass die Biene gezeichnet wird.
3. Füge das Unterprogramm `update` ein. Schreibe dort die entsprechenden Befehle hin, dass die Biene mit der Tastatur gesteuert werden kann.
4. Erweitere das Unterprogramm `update` so, dass die Biene immer in die richtige Richtung schaut.
***
``` python ./aufgabe_6.py
```
:::
