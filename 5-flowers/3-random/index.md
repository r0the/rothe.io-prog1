# Zufallsblumen
---

Mit dem Standardmodul `random` können Zufallszahlen erzeugt oder zufällig Werte aus Listen ausgewählt werden. Dazu muss das Modul ins Python-Script eingebunden werden. Dies geschieht mit folgender Zeile zu oberst im Programm:

``` python
import random
```

Mit Hilfe von `random.randrange()` kann eine zufällige ganze Zahl aus einem bestimmten Bereich ausgewählt werden.


Um eine zufällige ganze Zahl $n$ aus dem Bereich $0 \le n < b$ zu wählen, schreibt man:

``` python
n = random.randrange(b)
```

Um eine zufällige ganze Zahl $n$ aus dem Bereich $a \le n < b$ zu wählen, schreibt man:

``` python
n = random.randrange(a, b)
```

Um eine Blume zufällig auszuwählen, schreibt man:


``` python
blume = random.choice([veilchen, orchidee, loewenzahn])
```

::: exercise Aufgabe 4 – Zufallsblumen
1. Definiere das Unterprogramm `zeichne_zufallsblume` und füge die zwei Parameter die x- und y-Koordinate hinzu. Das Unterprogramm soll der gewünschten Stelle zufällig eine der drei Blumen platzieren und zeichnen. Dazu soll das schon vorhandene Unterprogramm `zeichne_blume` verwendet werden.
2. Rufe im Unterprogramm `draw` drei mal das neue Unterprogramm `zeichne_zufallsblume` auf.
3. Füge im Unterprogramm `draw` eine `for`-Schleife hinzu, so dass 20 Blumen gezeichnet werden. Bestimme dabei die x- und y-Koordinate zufällig – aber natürlich so, dass die Blumen auf dem Rasen zu liegen kommen.

![](./aufgabe_4.png)
***
``` python ./aufgabe_4.py
```
:::
