import pgzrun
import random

TITLE = "Blumenwiese"
WIDTH = 1000
HEIGHT = 733

wiese = Actor("wiese")
veilchen = Actor("veilchen")
loewenzahn = Actor("loewenzahn")
orchidee = Actor("orchidee")

def zeichne_blume(blume, x, y):
    blume.x = x
    blume.y = y
    blume.draw()

def zeichne_zufallsblume(x, y):
    blume = random.choice([veilchen, loewenzahn, orchidee])
    zeichne_blume(blume, x, y)

def draw():
    wiese.draw()
    for i in range(20):
        x = random.randrange(300, WIDTH - 50)
        y = random.randrange(400, HEIGHT - 50)
        zeichne_zufallsblume(x, y)

pgzrun.go()
