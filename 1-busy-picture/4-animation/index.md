# 1.4 Animation

Um in Pygame Zero eine Animation zu erstellen, muss ein zusätzliches Unterprogramm mit dem Namen `update` definiert werden:

Wenn ein Unterprogramm mit dem Namen `update` vorhanden ist, ändert Pygame Zero sein Verhalten. Nun wird erst `update` aufgerufen und anschliessend `draw`. Dies wird so lange wiederholt, bis das Programm abgebrochen wird.

Das entsprechende Python-Programm hat folgende Struktur:

``` python ./animation_template.py
```

## Eigenschaften animieren

Wir können die folgenden Eigenschaften eines Aktors animieren:

| Attribut  | Bedeutung                     |
|:--------- |:----------------------------- |
| `.x`      | x-Koordinate des Mittelpunkts |
| `.y`      | y-Koordinate des Mittelpunkts |
| `.left`   | linker Rand                   |
| `.right`  | rechter Rand                  |
| `.top`    | oberer Rand                   |
| `.bottom` | unterer Rand                  |
| `.angle`  | Drehwinkel                    |

Um Beispielsweise die Eigenschaft `angle` im Animationsschritt zu ändern, schreiben wir:

``` python
def update():
    figur1.angle = figur1.angle + 1
```

::: exercise
#### :exercise: Aufgabe 4 – Animation

Erweitere das Programm **wimmelbild.py** so, dass sich die Figuren bewegen und/oder drehen.

***
Hier ist ein Beispiel `update`-Unterprogramm, welches den Aktor `figur1` um die eigene Achse dreht:
``` python
def update():
    figur1.x = figur1.x + 1
```

***
``` python ./aufgabe4.py
```
:::
