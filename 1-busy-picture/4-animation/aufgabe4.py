import pgzrun

WIDTH = 800
HEIGHT = 600
hintergrund = Actor("hintergrund")
figur1 = Actor("figur1")
figur2 = Actor("figur2")


def update():
    figur1.angle = figur1.angle + 1


def draw():
    hintergrund.draw()
    figur1.draw()
    figur2.draw()

pgzrun.go()
