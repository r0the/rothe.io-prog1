# 3.3 Position
---

Als nächstes wollen wir die Fische im Bild positionieren. Die Position eines Aktors wird durch Variablen des Aktors bestimmt. Variablen eines Objekts werden auch **Attribute** genannt. Um auf ein Attribut zuzugreifen, verwenden wir wieder das Punkt. Um das Attribut `x` des Aktors `red_fish` zu ändern, schreiben wir:

``` python
red_fish.x = 200
```

Die folgende Tabelle zeigt die wichtigsten Attribute der Aktoren:

| Attribut  | Bedeutung                     |
|:--------- |:----------------------------- |
| `.x`      | x-Koordinate des Mittelpunkts |
| `.y`      | y-Koordinate des Mittelpunkts |
| `.left`   | linker Rand                   |
| `.right`  | rechter Rand                  |
| `.top`    | oberer Rand                   |
| `.bottom` | unterer Rand                  |
| `.image`  | Name der Bilddatei            |


## Koordinatensystem

In Pygame Zero wird ein in der Informatik übliches Koordinatensystem verwendet. Dabei ist der Ursprung in der linken oberen Ecke. Die x-Achse zeigt nach rechts, die y-Achse nach unten. Die Koordinaten werden in **Pixel** angegeben.

Die folgende Illustration veranschaulicht das Korrdinatensystem und die Positionierungsmöglichkeiten von Aktoren:

![Positionierung in Pygame Zero](./actor-position.svg)

## Rechnen

Die Werte für die Koordinaten müssen nicht als fixe Zahl angegeben werden, sie können auch berechnet werden. In Python werden die Grundrechenoperationen so geschrieben:

| Operation      | Mathematisch  | Python   |
| -------------- | ------------- | -------- |
| Addition       | $a + b$       | `a + b`  |
| Subtraktion    | $a - b$       | `a - b`  |
| Multiplikation | $a \cdot b$   | `a * b`  |
| Division       | $\frac{a}{b}$ | `a / b`  |
| Potenz         | $a^b$         | `a ** b` |

Ausserdem können vorhandene Variablen wie `HEIGHT` oder `red_fish.x` für die Berechnung verwendet werden.

**Beispiel:** Positioniere den `red_fish` in halber Höhe des Fensters:
``` python
red_fish.y = HEIGHT / 3
```

**Beispiel:** Positioniere den `red_fish` doppelt soweit vom oberen Rand wie vom linken Rand:
```python
red_fish.left = 20
red_fish.top = 2 * red_fish.left
```



::: exercise
#### :exercise: Aufgabe 3 – Positionierung

Füge weitere Objekte in das Bild ein und positioniere sie folgendermassen:

- ein Fisch (rot) am oberen Rand in der Mitte,
- ein Fisch (blau) am rechten Rand in der Mitte,
- ein Fisch (gelb) direkt links davon,
- ein Fisch in der Mitte des unteren linken Viertels.

Hier sind die zusätzlichen Fische zum herunterladen:

![](./images/fish_green_right.png)
![](./images/fish_yellow_right.png)

Das sollte etwa so aussehen:

![](./exercise-3.svg)

***
``` python ./aufgabe3.py
```

:::
